const { login, logout } = require('../controllers/auth-controller');
const router = require('express').Router();
const { validateFields } = require('../middlewares/validate-fields');
const { check } = require('express-validator');


router.post('/', [
    check('email').not().isEmpty().withMessage('Email is required'),
    check('email').isEmail().withMessage('Email is not valid'),
    check('password').not().isEmpty().withMessage('Password is required'),
    validateFields
], login);


router.get('/', logout);

module.exports = router;
