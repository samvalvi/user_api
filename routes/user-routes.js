const router = require('express').Router();
const { getUsers, getUserById, updateUser, deleteUser } = require('../controllers/user-controller');
const { validateFields } = require('../middlewares/validate-fields');
const { hasRole } = require('../middlewares/validate-role');
const { validateUserById } = require('../utils/validate-user');
const { validateToken } = require('../utils/validate-token');
const { check } = require('express-validator');


router.get('/', [
    validateToken,
    hasRole('ADMIN', 'USER')
],getUsers);


router.get('/:id', [
    hasRole('ADMIN', 'USER'),
    check('id').not().isEmpty().withMessage('Id is required'),
    check('id').isMongoId().withMessage('Id must be a valid MongoId'),
    check('id').custom(validateUserById),
    validateFields
], getUserById);


router.put('/:id', [
    hasRole('ADMIN', 'USER'),
    check('id').not().isEmpty().withMessage('Id is required'),
    check('id').isMongoId().withMessage('Id must be a valid MongoId'),
    check('id').custom(validateUserById),
    validateFields
], updateUser);


router.delete('/:id', [
    hasRole('ADMIN', 'USER'),
    check('id').not().isEmpty().withMessage('Id is required'),
    check('id').isMongoId().withMessage('Id must be a valid MongoId'),
    check('id').custom(validateUserById),
    validateFields
], deleteUser);


module.exports = router;
