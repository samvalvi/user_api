const router = require('express').Router();
const { refreshToken } = require('../controllers/token-controller');
const { validateRefreshToken } = require('../utils/validate-refreshToken');


router.get('/', [
    validateRefreshToken,
], refreshToken)


module.exports = router;
