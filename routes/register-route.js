const router = require('express').Router();
const { register } = require('../controllers/auth-controller');
const { validateFields } = require('../middlewares/validate-fields');
const { validateUserByEmail } = require('../utils/validate-user');
const { check } = require('express-validator');


router.post('/', [
    check('name').not().isEmpty().withMessage('Name is required'),
    check('email').not().isEmpty().isEmail().withMessage('Email is required'),
    check('email').custom(validateUserByEmail),
    check('password').not().isEmpty().withMessage('Password is required'),
    check('password').isLength({ min: 8 }).withMessage('Password must be at least 8 characters long'),
    validateFields
], register)


module.exports = router;
