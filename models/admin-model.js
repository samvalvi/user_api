const { Schema, model } = require('mongoose');


const adminSchema = new Schema({
    name: {
        type: String,
        required: [true, 'Name is required']
    },
    email: {
        type: String,
        required: [true, 'Email is required'],
        unique: true,
    },
    password: {
        type: String,
        required: [true, 'Password is required'],
        minlength: [8, 'Password must be at least 8 characters long']
    },
    role: {
        type: String,
        enum: ['ADMIN', 'USER'],
        default: 'ADMIN'
    },
    status: {
        type: String,
        enum: ['ACTIVE', 'INACTIVE'],
        default: 'ACTIVE'
    },
    refreshToken: {
        type: String,
        required: false
    }
});


adminSchema.methods.toJSON = function () {
    const { __v, _id, password, ...object } = this.toObject();
    object.uid = _id;
    return object;
}


module.exports = model('Admin', adminSchema);
