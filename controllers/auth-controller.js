const userSchema = require('../models/user-model');
const bcrypt = require('bcrypt');
const { createAccessToken, createRefreshToken } = require('../utils/create-token');


const register = async (req, res) => {
    const { name, email, password } = req.body; 

    const salt = await bcrypt.genSaltSync(13);
    const hash = await bcrypt.hashSync(password, salt);

    const user = new userSchema({
        name: name,
        email: email,
        password: hash
    });

    await user.save();

    return res.status(201).json({
        statusCode: 201,
        message: 'User created successfully'
    });
}


const login = async (req, res) => {
    const { email, password } = req.body;
    const query = { email: email, status: 'ACTIVE' };

    const user = await userSchema.findOne(query);

    if (!user) {
        return res.status(401).json({
            statusCode: 401,
            message: 'Credentials are invalid'
        });
    }

    const isPasswordValid = await bcrypt.compareSync(password, user.password);

    if (!isPasswordValid) {
        return res.status(401).json({
            statusCode: 401,
            message: 'Credentials are invalid'
        });
    }

    const [accessToken, refreshToken] = await Promise.all([
        createAccessToken(user.id),
        createRefreshToken(user.id, res)
    ]);
    

    return res.status(200).json({
        statusCode: 200,
        message: 'Login successful',
        accessToken,
    });
}


const logout = (req, res) => {
    res.clearCookie('refreshToken');
    
    return res.status(200).json({
        statusCode: 200,
        message: 'Logout successful'
    });
}


module.exports = {
    register,
    login,
    logout
}
