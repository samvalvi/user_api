const { createAccessToken } = require('../utils/create-token');
const jwt = require('jsonwebtoken');


const refreshToken = (req, res) => {
    try {
        const token = req.cookies.refreshToken;

        if (!token) {
            return res.status(401).json({
                statusCode: 401,
                message: 'No token provided'
            });
        }

        //const { id } = jwt.verify(token, process.env.JWT_REFRESH);

        const accessToken = createAccessToken(req.id);

        return res.json(accessToken);

    }catch(err){
        console.error(err);
        res.status(500).json({
            statusCode: 500,
            message: 'Internal server error'
        });
    }
}


module.exports = { refreshToken };
