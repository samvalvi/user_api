const userSchema = require('../models/user-model');


const getUsers = async (req, res) => {
    
    const query = {status: 'ACTIVE'};
   
    const [total, users] = await Promise.all([
        userSchema.countDocuments(query),
        userSchema.find(query)
    ])

    res.status(200).json({
        statusCode: 200,
        message: 'Get users',
        data: {
            "records": total,
            users
        }
    });
}


const getUserById = async (req, res) => {
    const { id } = req.params;

    const user = await userSchema.findById(id);

    res.status(200).json({
        statusCode: 200,
        message: 'User fetched successfully',
        data: {
            user
        }
    });
}


const updateUser = async (req, res) => {
    const { id } = req.params;
    const { name, email, role } = req.body;

    const user = await userSchema.findByIdAndUpdate(id, { name, email, role }, { new: true });

    res.status(200).json({
        statusCode: 200,
        message: 'User updated successfully',
        data: {
            user
        }
    });
}


const deleteUser = async (req, res) => {
    const { id } = req.params;

    const user = await userSchema.findByIdAndUpdate(id, { status: 'INACTIVE' });

    res.status(200).json({
        statusCode: 200,
        message: 'User deleted successfully',
        data: {
            user
        }
    });
}


module.exports = {
    getUsers,
    getUserById,
    updateUser,
    deleteUser
};
