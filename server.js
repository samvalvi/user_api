const express = require('express');
require('dotenv').config();
const cors = require('cors');
const { getConnection } = require('./database/config');
const cookieParser = require('cookie-parser');


const white_list = [process.env.ORIGIN];

class Server {
    constructor() {
        this.app = express();
        this.port = process.env.PORT || 3000;
        this.tokenPath = '/api/refresh-token';
        this.userPath = '/api/user';
        this.loginPath = '/api/login';
        this.registerPath = '/api/register';


        this.middlewares();

        this.database();

        this.start();
    }


    database() {
        getConnection();
    }


    middlewares() {

        this.app.use(cors({
            origin: function (origin, callback) {
                if(white_list.includes(origin)) {
                    return callback(null, origin);
                }
                return callback(new Error('Not allowed by CORS'));
            }
        }));

        this.app.use(express.json());
        this.app.use(cookieParser());
    }


    start() {
        this.app.use(this.tokenPath, require('./routes/token-routes'));
        this.app.use(this.registerPath, require('./routes/register-route'));
        this.app.use(this.loginPath, require('./routes/login-route'));
        this.app.use(this.userPath, require('./routes/user-routes'));
    }

    
    listen() {
        this.app.listen(this.port, () => {
            console.log(`Server is running on port ${this.port}`);
        });
    }
}


module.exports = { Server }
