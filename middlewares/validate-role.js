const { response, request } = require("express")


const hasRole = (...roles) => {
    return (req=request, res=response, next) => {

        if(!req.user){
            return res.status(401).json({
                statusCode: 401,
                message: 'Forbidden access to this resource without authorization token'
            });
        }
        
        const { role } = req.user;

        if (!roles.includes(role)) {
            return res.status(401).json({
                statusCode: 401,
                message: "Unauthorized"
            });
        }
        next();
    }
}


module.exports = { hasRole };
