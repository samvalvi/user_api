const jwt = require("jsonwebtoken");
require("dotenv").config();


const createAccessToken = (id) => {

    try{
        const access_token = jwt.sign({ id }, process.env.JWT_SECRET, { expiresIn: "15m" });
        return access_token;

    }catch(err){
        console.log(err);
        throw new Error("Error creating access token");
    }
}


const createRefreshToken = (id, res) => {
        
        try {
            const refresh_token = jwt.sign({ id }, process.env.JWT_REFRESH, { expiresIn: '7d' });
            res.cookie('refresh_token', refresh_token, {
                httpOnly: true,
                secure: !(process.env.MODE_ENV === 'development'),
                expires: new Date(Date.now() + 60 * 60 * 24 * 7 * 1000),
                sameSite: 'none',
            });

        }catch(err){
            console.log(err);
            throw new Error("Error creating refresh token");
        }
}   


module.exports = {
    createAccessToken,
    createRefreshToken
}
