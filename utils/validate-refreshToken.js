const jwt = require('jsonwebtoken');


const validateRefreshToken = (req, res, next) => {
    try{

        const refreshToken = req.cookies.refreshToken;
    
        if(!refreshToken) {
            return res.status(401).json({
                statusCode: 401,
                message: 'No refresh token provided'
            });
        }
        
        const { id } = jwt.verify(refreshToken, process.env.JWT_REFRESH);

        req.id = id;
        
        next();

    }catch(err) {
        console.log(err);
        res.status(401).json({
            statusCode: 401,
            message: 'Invalid refresh token'
        });
    }
}


module.exports = { validateRefreshToken };
