const userSchema = require("../models/user-model");


const validateUserById = async (id) => {
    const user = await userSchema.findById(id);

    if (!user) {
        throw new Error('User not found');
    }
}



const validateUserByEmail = async (email) => {
    const query = { email: email, status: 'ACTIVE' };
    const user = await userSchema.findOne(query);

    if (user) {
        throw new Error('User already exists');
    }
}


module.exports = {
    validateUserById,
    validateUserByEmail
}
