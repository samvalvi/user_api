const jwt = require("jsonwebtoken");
const userSchema = require("../models/user-model");


const validateToken = async (req, res, next) => {
    try {

        const token = req.header('authorization');

        if(!token) {
            return res.status(401).json({
                statusCode: 401,
                message: 'No token provided'
            });
        }

        const { id } = jwt.verify(token, process.env.JWT_SECRET);
        
        const user = await userSchema.findById(id);

        req.user = user;

        next();

    }catch(err){
        return res.status(401).json({
            statusCode: 401,
            message: 'Invalid authorization token'
        });
    }
}

module.exports = { validateToken };
