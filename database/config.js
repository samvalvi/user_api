const mongoose = require('mongoose');


const options = {
    maxPoolSize: 10,
}

const getConnection = async () => {
    try {
        await mongoose.connect(process.env.MONGODB_URL, options, {
            useNewUrlParser: true,
            useUnifiedTopology: true,
            useCreateIndex: true
        });
        console.log('Database is connected');

    }catch(err){
        console.log(err);
        throw new Error('Database is not connected');
    }
}

module.exports = { getConnection };
